<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('home');
});

Route::get('/', 'HomeController@home');

Route::get('/Register', 'AuthController@register');

Route::get('/Welcome', 'AuthController@welcome');
Route::post('/Welcome', 'AuthController@welcome_post');

Route::get('/casts/create', 'CastsController@create');
Route::post('/casts', 'CastsController@store');
Route::get('/casts', 'CastsController@index');  
Route::get('/casts/{id}', 'CastsController@show');  
Route::get('/casts/{id}/edit', 'CastsController@edit');
Route::put('/casts/{id}', 'CastsController@update');
Route::delete('/casts/{id}', 'CastsController@destroy');



