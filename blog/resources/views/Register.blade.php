<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>

    <h3>Sign Up Form</h3>

    <form action="/Welcome" method="POST">
    @csrf
    <label for="fName">First name:</label><br><br>
    <input type="text" id="fName" name="nama"><br><br>

    <label for="lName">Last name:</label><br><br>
    <input type="text" id="lName" name="namaPanjang"><br><br>
        
    <label>Gender:</label><br><br>

    <input type="radio" id="male" value="M" name="gender">
    <label for="male">Male</label><br>

    <input type="radio" id="female" value="F" name="gender">
    <label for="female">Female</label><br>

    <input type="radio" id="other" value="O" name="gender">
    <label for="other">Other</label>
        
    <br>
    <label for="national">Nationality:</label><br><br>
    <select name="inputnational" id="national">
        <option value="Indonesia">Indonesian</option>
        <option value="Singapore">Singaporean</option>
        <option value="Malaysia">Malaysian</option>
        <option value="Australia">Australian</option>
    </select>
        
    <br>
    <label>Language Spoken:</label><br><br>
    <input type="checkbox" name="bahasa" id="bahasa">
    <label for="bahasa">Bahasa Indonesia</label><br>
    <input type="checkbox" name="bahasa" id="english">
    <label for="english">English</label><br>
    <input type="checkbox" name="bahasa" id="otherLanguage">
    <label for="otherLanguage">Other</label>
        
    <br>
    <label for="bio">Bio:</label><br>
    <textarea id="" cols="30" rows="10"></textarea><br>
    <input type="submit" value="Sign Up">

    </form>

</body>

</html>