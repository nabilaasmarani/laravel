@extends('master')

@section('content')
<div class="card">
    <div class="card-header">
      <h3 class="card-title">List Casts</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success')}}
            </div>
        @endif
        <a class="btn btn-primary mb-2" href="/casts/create">Add New Cast</a>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th style="width: 50px">#</th>
            <th style="width: 400px">Nama</th>
            <th style="width: 50px" class="text-center">Umur</th>
            <th>Bio</th>
            <th style="width: 200px">Action</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($casts as $item => $casts)
              <tr>
                  <td>{{$item + 1 }}</td>
                  <td>{{$casts -> nama }}</td>
                  <td class="text-center"> {{$casts -> umur }} </td>
                  <td>{{$casts -> bio }}</td>
                  <td style="display: flex;">
                      <a href="/casts/{{$casts->id}}" class="btn btn-info text-light">Show</a>
                      <a href="/casts/{{$casts->id}}/edit" class="btn btn-warning"><i class="bi bi-pencil"></i></a>
                      <form action="/casts/{{$casts->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" value="delete" class="btn btn-danger text-dark">
                      </form>
                  </td>
              </tr>
              @empty
              <tr>
                <td colspan="5" align="center">No Casts</td> 
              </tr>
          @endforelse
        </tbody>
      </table>
    </div>
    <!-- /.card-body -->
  </div>
@endsection